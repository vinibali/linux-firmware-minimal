# linux-firmware-minimal

  Minimal amount of firmware files for Linux, mostly recommended for virtual systems.
  To be precise this the first release of this package.
  Arch Linux user repository package: [linux-firmware-minimal](https://aur.archlinux.org/packages/linux-firmware-minimal/)
